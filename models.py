from app import db
from flask_login import UserMixin
from werkzeug.security import generate_password_hash, check_password_hash  # 生成密码散列值函数

# 创建数据模型对象

class User(UserMixin, db.Model):  # 用户
    id = db.Column(db.Integer, primary_key=True)
    userID = db.Column(db.Integer, nullable=False)
    password = db.Column(db.String(128), nullable=False)
    phone = db.Column(db.BigInteger, nullable=False, unique=True)
    email = db.Column(db.String(50), nullable=False, unique=True)
    active = db.Column(db.Boolean())

    def set_password(password):  # 用来设置密码的方法，接受密码作为参数
        return generate_password_hash(password)  # 将生产的密码保持到对应的字段

    def validate_password(self, password):  # 用于验证密码的方法，接受密码作为参数
        return check_password_hash(self.password, password)
        # 返回布尔值


# db.create_all()