from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_login import LoginManager
# 实例初始化环境信息文件
app = Flask(__name__)

app.config['SECRET_KEY'] = 'xxx'  # 等同于 app.secret_key = 'dev'  设置签名所需的钥匙

login_manager = LoginManager()
login_manager.init_app(app)
# login_manager = LoginManager(app)  # 实例化扩展类

app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql://li:Lisonglin-123@rm-bp1754r03lr373u006o.mysql.rds.aliyuncs.com/user'
# 格式 mysql://名称:密码@IP/数据库
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
# 关闭对模型修改的监控

app.jinja_env.auto_reload = True
app.config['TEMPLATES_AUTO_RELOAD'] = True
# 热更新HTML模板文件

db = SQLAlchemy(app)
# 在扩展类实例化前加载配置
login_manager.login_view = 'login'  # 定义错误提示信息
login_manager.login_message_category = 'info'
login_manager.login_message = 'Access denied.'

@login_manager.user_loader
def load_user(user_id):  # 创建用户加载回调函数，接受用户ID作为参数
    from models import User
    user = User.query.get(int(user_id))  # 用ID作为user模型的主键查询对应的用户
    return user  # 返回用户对象


import views, errors, forms
